"use strict";

const path = require('path');

const __root = path.join(__dirname, '..');

const dirs = {
    animated: path.join(__root, 'animated'),
    animated_fixed: path.join(__root, 'animated-fixed')
};

const fs = require('fs');

const files = fs.readdirSync(dirs.animated);

const resizes = [];

const imagemin = require('imagemin');
const imageminGifsicle = require('imagemin-gifsicle');

let fileNext = function(){
    if(files.length > 0){
        let fileName = files.shift();
        let file = path.join(dirs.animated, fileName);
        let fileOut = path.join(dirs.animated_fixed, fileName);

        console.log('File Out: '+fileOut);

        if(fs.existsSync(fileOut)){
            console.log('Cached: '+fileOut);
            fileNext();
        }else{
            console.log('Running: '+fileOut);
            imagemin([file], dirs.animated_fixed, {
                plugins: [imageminGifsicle({
                    optimizationLevel: 3
                })],
            })
                .then(function(){
                    console.log('Images optimized');
                    fileNext();
                });
        }
    }else{
        console.log('done!');
        process.exit(0);
    }
};

fileNext();